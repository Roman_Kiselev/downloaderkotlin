package ru.manage.impl

import ru.download.CopyFile
import ru.download.DownloadResult
import ru.manage.interf.DownloadManager

class DownloadManagerCopeFile(val fileDestination: String, val fileSource:String,
                              val downloadResult: DownloadResult): DownloadManager {

    override fun download(): Int {
        val copyFile = CopyFile(fileDestination, fileSource, downloadResult)
        return copyFile.call()
    }
}