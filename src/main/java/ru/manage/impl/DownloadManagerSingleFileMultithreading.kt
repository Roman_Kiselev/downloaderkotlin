package ru.manage.impl

import ru.download.DownloadGuard
import ru.download.DownloadResult
import ru.download.FilePerThreadDownloader
import ru.entity.DownloadItem
import ru.manage.interf.DownloadManager
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class DownloadManagerSingleFileMultithreading(val downloadItems: List<DownloadItem>, val count: Int,
                                              val downloadGuard: DownloadGuard, val downloadResult: DownloadResult):DownloadManager {

    override fun download(): Int {
        val executorService = Executors.newFixedThreadPool(count)
        val result = ArrayList<Future<Int>>()
        downloadResult.result = false
        downloadItems.forEach { result.add(executorService.submit(FilePerThreadDownloader(downloadGuard, it.fileName,
                it.url, downloadResult))) }
        executorService.shutdown()
        executorService.awaitTermination(java.lang.Long.MAX_VALUE, TimeUnit.NANOSECONDS)
        var r: Int = 0
        result.forEach{it -> r += it.get()}
        return r
    }
}