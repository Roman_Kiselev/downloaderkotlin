package ru.manage.impl

import ru.download.SingleFileDownloader
import ru.entity.DownloadItem
import ru.manage.interf.DownloadManager
import java.util.concurrent.Executors
import kotlin.streams.toList

class DownloadManagerSingleFile(val downloadItems: List<DownloadItem>): DownloadManager {
    override fun download(): Int {
        val exec = Executors.newFixedThreadPool(downloadItems.size)
        val result = downloadItems.stream().map { downloadItem -> exec.submit(SingleFileDownloader(
                downloadItem.fileName, downloadItem.url)) }.toList()
        exec.shutdown()
        return result
                .map { it.get()}
                .sum()
    }
}