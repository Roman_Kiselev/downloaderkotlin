package ru.manage.impl

import ru.download.DownloadGuard
import ru.download.DownloadResult
import ru.download.PartitionDownloader
import ru.entity.DownloadItem
import ru.manage.interf.DownloadManager
import java.net.URL
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class DownloadManagerPartition(val downloadItems: List<DownloadItem>, val count: Int, val speed: Int,
                               val downloadResult: DownloadResult): DownloadManager {
    override fun download(): Int {
        var r = 0
        for (downloadItem in downloadItems){
            downloadResult.result = false
            val website = URL(downloadItem.url)
            val connection = website.openConnection()
            connection.connect()
            val currentSize = connection.contentLength
            val residue = currentSize % count
            var begin: Int = 0
            var end: Int = 0
            var step: Int = 0
            if (residue != 0){
                step = currentSize / (count - 1)
                end = currentSize
                val resideReCalc = if (currentSize % (count - 1) == 0) residue else currentSize % (count - 1)
                begin = currentSize - resideReCalc
            } else run {
                step = currentSize / count
                begin = currentSize - step
                end = currentSize
            }
            val result = ArrayList<Future<Int>>()
            val executorService = Executors.newFixedThreadPool(count)
            val guard = DownloadGuard(speed)
            for (i in 0..count - 1) {
                result.add(executorService.submit(PartitionDownloader(end, begin, downloadItem.fileName,
                        downloadItem.url, guard, downloadResult)))
                end = begin
                begin -= step
            }
            executorService.shutdown()

            executorService.awaitTermination(java.lang.Long.MAX_VALUE, TimeUnit.NANOSECONDS)
            for (resultItem in result) {
                r += resultItem.get()
            }
            downloadResult.resultHashMap.put(downloadItem.fileName, true)
        }
        return r
    }
}