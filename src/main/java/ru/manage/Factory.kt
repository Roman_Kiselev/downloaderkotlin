package ru.manage

import ru.constant.DownloadType
import ru.download.DownloadGuard
import ru.download.DownloadResult
import ru.entity.DownloadItem
import ru.manage.impl.DownloadManagerCopeFile
import ru.manage.impl.DownloadManagerPartition
import ru.manage.impl.DownloadManagerSingleFile
import ru.manage.impl.DownloadManagerSingleFileMultithreading
import ru.manage.interf.DownloadManager
import java.util.*

object Factory {
    private val items = HashMap<String, String>()
    private var guard: DownloadGuard? = null
    fun getManager(downloadItems: List<DownloadItem>, count: Int, speed: Int,
                   type: DownloadType): List<DownloadManager> {
        var result: DownloadManager? = null
        val downloadResult = DownloadResult()
        val resultList = ArrayList<DownloadManager>()
        val finalResult = ArrayList<DownloadManager>()
        val doubleItems = ArrayList<DownloadItem>()
        var found: Boolean
        //search for duplicate files
        for (downloadItem in downloadItems) {
            val iterator = items.entries.iterator()
            found = false
            var item: Map.Entry<String, String>
            while (iterator.hasNext() && !found) {
                item = iterator.next()
                if (downloadItem.url == item.value) {
                    found = true
                    resultList.add(DownloadManagerCopeFile(downloadItem.fileName, item.key, downloadResult))
                    doubleItems.add(downloadItem)
                }
            }
            items.put(downloadItem.fileName, downloadItem.url)
        }
        //remove duplicate files
        val freeItems = downloadItems.filter { downloadItem -> !doubleItems.contains(downloadItem) }.toList()
        if (freeItems.isNotEmpty()) {
            when (type) {
                DownloadType.PARTITION -> result = DownloadManagerPartition(freeItems, count, speed, downloadResult)
                DownloadType.SINGLE -> result = DownloadManagerSingleFile(freeItems)
                DownloadType.FILE_PER_THREAD -> {
                    if (guard == null) guard = DownloadGuard(speed)
                    result = DownloadManagerSingleFileMultithreading(freeItems, count, guard!!, downloadResult)
                }
            }
            finalResult.add(result)
        }
        finalResult.addAll(resultList)
        return finalResult
    }

}