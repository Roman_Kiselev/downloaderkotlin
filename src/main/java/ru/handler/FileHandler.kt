package ru.handler

import ru.entity.DownloadItem
import java.io.BufferedReader
import java.io.FileReader




class FileHandler(val fileName: String, val fileDestination: String) {
    fun prepareFile(): List<DownloadItem> {
        val list = ArrayList<String>()
        val result = ArrayList<DownloadItem>()
        BufferedReader(FileReader(fileName)).use {
            var str :String? = it.readLine()
            while (str  != null) {
                list.add(str)
                str = it.readLine()
            }
        }

        for(item in list){
            val index = item.indexOf(" ")
            val url = item.substring(0, index)
            val name = item.substring(index + 1, item.length)
            result.add(DownloadItem(url, fileDestination + name))
        }
        return result
        /*Files.lines(Paths.get(fileName)).use {
            return it.toList().map { string -> {
                val index = string.indexOf(" ")
                val url = string.substring(0, index)
                val name = string.substring(index + 1, string.length)
                DownloadItem(url, fileDestination + name)
            }}.toList()

        }*/
    }
}