package ru.download

import java.io.File
import java.io.FileInputStream
import java.nio.file.Files
import java.util.concurrent.Callable

class CopyFile(val fileDestination: String, val fileSource: String, val  downloadResult: DownloadResult): Callable<Int>{
    private val TWO_SECOND = 2000L

    override fun call():Int{
        var currentSize = 0L
        val source = File(fileSource)
        var isDone = downloadResult.resultHashMap[fileSource]
        while(isDone == null){
            Thread.sleep(TWO_SECOND)
            isDone = downloadResult.resultHashMap[fileSource]
        }
        if (!isDone){
            println("$fileSource not downloaded")
            return 0
        }
        FileInputStream(source).use {
            currentSize = it.channel.size()
        }
        var oldSize = 0L
        while(currentSize != oldSize){
            FileInputStream(File(fileSource)).use {
                oldSize = it.channel.size()
            }
            Thread.sleep(TWO_SECOND)
        }
        Files.copy(File(fileSource).toPath(), File(fileDestination).toPath())
        return 0
    }
}
