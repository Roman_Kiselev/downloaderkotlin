package ru.download

import java.io.BufferedInputStream
import java.io.File
import java.io.RandomAccessFile
import java.net.URL
import java.util.concurrent.Callable

class FilePerThreadDownloader(val downloadGuard: DownloadGuard, val fileName: String, val fileUrl: String,
                              val downloadResult: DownloadResult): Callable<Int> {
    private val BUFFER_SIZE = 1024
    override fun call(): Int{
        try {
            RandomAccessFile(File(fileName), "rw").use { targetFile ->
                targetFile.seek(0)
                val buf = ByteArray(BUFFER_SIZE)
                var size = 0
                val url = URL(fileUrl)
                val urlConnection = url.openConnection()
                BufferedInputStream(urlConnection.getInputStream()).use {
                    var done = false
                    while (!done) {
                        val sleep = downloadGuard.canI()
                        if (sleep > 0) {
                            Thread.sleep(sleep)
                        }
                        val len = it.read(buf, 0, BUFFER_SIZE)
                        if (len == -1) {
                            done = true
                        } else {
                            targetFile.write(buf, 0, len)
                            size += len
                        }
                        downloadGuard.downloaded(len.toLong())
                    }
                }
                downloadResult.resultHashMap.put(fileName, true)
                return size
            }
        }catch (e: Exception){
            println(e)
            downloadResult.resultHashMap.put(fileName, false)
            return 0
        }

    }
}