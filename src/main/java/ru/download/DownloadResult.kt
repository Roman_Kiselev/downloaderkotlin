package ru.download

import java.util.concurrent.ConcurrentHashMap

class DownloadResult {
    val resultHashMap: MutableMap<String, Boolean> = ConcurrentHashMap()
    var result: Boolean  = true
}