package ru.download

import java.io.BufferedInputStream
import java.io.File
import java.io.RandomAccessFile
import java.net.URL
import java.util.concurrent.Callable

class PartitionDownloader(val endIndex: Int, var currentIndex: Int, val fileName: String, val fileUrl: String,
                          val downloadGuard: DownloadGuard, val downloadResult: DownloadResult): Callable<Int>{
    private val BUFFER_SIZE = 1024
    override fun call(): Int {
        val targetFile = File(fileName)
        synchronized(PartitionDownloader::class.java){
            if (!targetFile.exists()){
                if(!targetFile.createNewFile()){
                    println("Already created $fileName")
                }
            }
        }
        val buf = ByteArray(BUFFER_SIZE)
        if (currentIndex < 0){
            if (endIndex == 0) return 0
            else currentIndex = 0
        }
        RandomAccessFile(targetFile, "rw").use { randomAccessFile ->
            val url = URL(fileUrl)
            var size = 0
            val urlConnection = url.openConnection()
            urlConnection.setRequestProperty("Range", "bytes=$currentIndex-$endIndex")
            randomAccessFile.seek(currentIndex.toLong())
            try {
                BufferedInputStream(urlConnection.getInputStream()).use {
                    var done = false
                    while (currentIndex < endIndex && !done && !downloadResult.result) {
                        val sleep = downloadGuard.canI()
                        if (sleep > 0) {
                            Thread.sleep(sleep)
                        }
                        val len = it.read(buf, 0, BUFFER_SIZE)
                        if (len == -1) {
                            done = true
                        } else {
                            randomAccessFile.write(buf, 0, len)
                            currentIndex += len
                            size += len
                        }
                        downloadGuard.downloaded(len.toLong())
                    }
                    return size
                }
            }catch (e: Exception){
                println(e)
                downloadResult.result = false
                return 0
            }
        }
    }
}