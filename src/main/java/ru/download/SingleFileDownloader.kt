package ru.download

import java.io.FileOutputStream
import java.net.URL
import java.nio.channels.Channels
import java.util.concurrent.Callable

class SingleFileDownloader(val fileName: String, val url: String): Callable<Int> {
    override fun call(): Int {
        val website = URL(url)
        val rbc = Channels.newChannel(website.openStream())
        val fos = FileOutputStream(fileName)
        fos.channel.transferFrom(rbc, 0, Long.MAX_VALUE)
        return fos.channel.size().toInt()
    }
}