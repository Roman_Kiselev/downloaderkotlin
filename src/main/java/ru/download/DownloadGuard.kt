package ru.download

class DownloadGuard(val speed: Int) {
    private var startTime: Long = System.currentTimeMillis()
    var size: Long = 0
    private val SECOND: Long = 1000

    @Synchronized
    fun canI():Long{
        val currentTime = System.currentTimeMillis()
        val dif = currentTime - startTime
        if (dif < SECOND){
            if (size > speed){
                return SECOND - dif
            }else return 0
        }else{
            startTime = System.currentTimeMillis()
            size = 0
            return 0
        }
    }

    @Synchronized
    fun downloaded(downloaded: Long){
        size += downloaded
    }

}