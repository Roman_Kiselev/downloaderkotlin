package ru.constant

enum class DownloadType {
    SINGLE,
    PARTITION,
    FILE_PER_THREAD
}