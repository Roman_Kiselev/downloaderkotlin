
import ru.constant.DownloadType
import ru.handler.FileHandler
import ru.manage.Factory
import ru.manage.interf.DownloadManager
import java.io.File
import kotlin.system.exitProcess

fun main(arg: Array<String>){
    var fileName: String? = null
    var destinationDir: String? = null
    var threadCount: Int = 0
    var speed: String? = null
    var count: Int = 0
    while (count < arg.size){
        when(arg[count]){
            "-n" -> {count++; threadCount = arg[count].toInt()}
            "-l" -> {count++; speed = arg[count]}
            "-f" -> {count++; fileName = arg[count]}
            "-o" -> {count++; destinationDir = arg[count]}
        }
        count++
    }
    if (destinationDir == null){
        println("Need for destination dir")
        exitProcess(0)
    }
    if (destinationDir.lastIndexOf('\\') != destinationDir.length){
        destinationDir += "\\"
    }
    val dir = File(destinationDir)
    if (!dir.exists()){
        if (!dir.mkdirs()){
            println("cannot create dirs")
            exitProcess(0)
        }
    }
    if (speed == null){
        println("Need for speed")
        exitProcess(0)
    }
    val length = speed.length
    val limitSpeed: Int
    if (Character.isLetter(speed[length - 1])){
        when(speed.substring(length - 1, length)){
            "k" -> {limitSpeed = speed.substring(0, length - 1).toInt() * 1024}
            "m" -> {limitSpeed = speed.substring(0, length - 1).toInt() * 1048576}
            else -> limitSpeed = speed.substring(0, length - 1).toInt()
        }
    }else{
        limitSpeed = speed.toInt()
    }

    if (fileName == null){
        println("Need for file name")
        exitProcess(0)
    }

    val fileHandler = FileHandler(fileName, destinationDir)
    val items = fileHandler.prepareFile()
    if (threadCount == 0){
        println("Need for thread count")
        exitProcess(0)
    }
    val type = DownloadType.FILE_PER_THREAD
    val result = Factory.getManager(items, threadCount, limitSpeed, type)
    val begin = System.currentTimeMillis()
    val downloaded = result.map(DownloadManager::download).sum()
    val end = System.currentTimeMillis()
    println("Passed ${(end - begin) / 1000.0F}  seconds")
    println("downloaded $downloaded bytes")

}